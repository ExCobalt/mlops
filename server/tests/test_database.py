
import pytest

from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL


CONF_DB = {
    'database'      : 'postgres',
    'user'          : 'postgres',
    'host'          : 'localhost',
    'port'          : '5432',
    'password'      : 'password',
    'container_name': 'postgres',
}


@pytest.fixture(scope='session')
def db_connection():
    url = URL.create(
        "postgresql+psycopg2",
        username=CONF_DB.get('user'),
        password=CONF_DB.get('password'),
        host=CONF_DB.get('host'),
        port=CONF_DB.get('port'),
        database=CONF_DB.get('database'),
    )
    dbc = create_engine(url)
    return dbc


def test_db_connection(db_connection):
    with db_connection.begin() as conn:
        result = conn.execute('SELECT schema_name FROM information_schema.schemata').all()
        result = [item for subresult in result for item in subresult]
    assert 'public' in result
