
import pytest
from mock import patch, Mock
import numpy as np
import pandas as pd

from models.survival import Model


def mock_connect_db():
    return None


def mock_get_data():
    data = pd.read_csv('./support/survival_test_data.csv', sep=';', decimal=',')

    data['dtAgrmntOpen'] = pd.to_datetime(data['dtAgrmntOpen']) + pd.offsets.MonthEnd(0)
    data['dtAgrmntClose'] = pd.to_datetime(data['dtAgrmntClose']) + pd.offsets.MonthEnd(0)

    data['dtReport'] = (
        data['dtAgrmntOpen'].values.astype('datetime64[M]').astype(np.int64)
        + data['nMonth'] - 1
    ).astype('datetime64[M]') + pd.offsets.MonthEnd(0)

    data['dmIsExpired'] = (data['dtReport'] >= data['dtAgrmntClose']).astype(np.int8)

    for i in ['ctgrCardLevel', 'ctgrSalesChannel', 'dmMassOffer', 'nIsPayroll']:
        data[i] = data[i].astype(np.int8)

    return data


@patch.object(Model, 'connect_db', return_value=mock_connect_db())
@patch.object(Model, 'get_data', return_value=mock_get_data())
def test_logit(*args):
    model = Model()
    model.set_model_type('Logit')
    spec = ['ctgrCardLevel', 'ctgrSalesChannel', 'dmMassOffer', 'nIsPayroll']
    result = model.fit_logit(specification=spec, fit_intercept=True)
    assert result.aic > 0


@patch.object(Model, 'connect_db', return_value=mock_connect_db())
@patch.object(Model, 'get_data', return_value=mock_get_data())
def test_km(*args):
    model = Model()
    model.set_model_type('Kaplan-Meier')
    result = model.fit_km()
    assert result.survival_function_.shape[0] > 0


@patch.object(Model, 'connect_db', return_value=mock_connect_db())
@patch.object(Model, 'get_data', return_value=mock_get_data())
def test_cox(*args):
    model = Model()
    model.set_model_type('Cox')
    spec = ['ctgrCardLevel', 'ctgrSalesChannel', 'dmMassOffer', 'nIsPayroll']
    result = model.fit_cox(id_col='nAgrmntId', stop_col='nMonth', specification=spec)
    assert result.baseline_survival_.shape[0] > 0


@patch.object(Model, 'connect_db', return_value=mock_connect_db())
@patch.object(Model, 'get_data', return_value=mock_get_data())
def test_fit(*args):
    model = Model()
    spec = ['ctgrCardLevel', 'ctgrSalesChannel', 'dmMassOffer', 'nIsPayroll']
    model.set_model_type('Logit')
    model.fit(spec, fit_intercept=True)
    model.set_model_type('Kaplan-Meier')
    model.fit_km()
    model.set_model_type('Cox')
    model.fit_cox(id_col='nAgrmntId', stop_col='nMonth', specification=spec)
    assert True
